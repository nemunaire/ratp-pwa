const cacheName = "v1";

self.addEventListener("install", function(e) {
    e.waitUntil(
	caches.open(cacheName).then(function(cache) {
	    return cache.addAll([
		'/',
		'/style.css',
		'/app.js',
		'/jquery-3.3.1.min.js',
		'/favicon.ico',
		'/fonts/Parisine-Bold.eot',
		'/fonts/Parisine-BoldItalic.eot',
		'/fonts/Parisine-BoldItalic.woff',
		'/fonts/Parisine-Bold.woff',
		'/fonts/Parisine-Italic.ttf',
		'/fonts/Parisine-Italic.woff',
		'/fonts/Parisine-Regular.eot',
		'/fonts/Parisine-Regular.woff',
		'/bootstrap.min.css',
		'/logo.svg',
	    ]);
	})
    )
})

self.addEventListener("activate", function(e) {
    e.waitUntil(
	caches.keys().then(function(cacheNames) {
	    return Promise.all(cacheNames.map(function(cName) {
		if (cName != cacheName) {
		    return caches.delete(cName);
		}
	    }))
	})
    )
})

self.addEventListener("fetch", function(e) {
    var url = e.request.url.split("/");

    var nreq = e.request.clone();
    if (url[3] == "metros" || url[3] == "rers" || url[3] == "bus" || url[3] == "tramways" || url[3] == "noctiliens") {
	nreq = new Request(url.slice(0,3).join("/") + "/");
    }

    e.respondWith(
	caches.match(nreq)
	    .then(function(response) {
		if (response) {
		    // Return the cached version
		    return response;
		}

		var requestClone = nreq.clone();
		return fetch(requestClone)
		    .then(function(response) {

			if(!response || response.type !== 'basic') {
			    return response;
			}

			if (!(url[3] == "api" && (url[4] == "traffic" || url[4] == "schedules"))) {
			    var responseClone = response.clone();
			    caches.open(cacheName).then(function(cache) {
				cache.put(nreq, responseClone);
			    });
			}

			// Return the response
			return response;
		    })
	    })
    );
})
