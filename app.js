const BASE_API = "/api";

var nextSchedule = null;
var nextTraffic = null;

var uri = window.location.pathname.split("/").filter(function(v) {
    return v.length > 0;
});

$("#linesFilter").change(function() {
    if ($("#linesFilter").val().length != 0) {
	findLines($("#linesFilter").val());
    }
});

$("#line").change(function() {
    if ($("#line").val().length != 0) {
	findDestinations($("#linesFilter").val(), $("#line").val());
	findStations($("#linesFilter").val(), $("#line").val());
	getTraffic($("#linesFilter").val(), $("#line").val());
    }
});

$("#stations").change(function() {
    if ($("#stations").val().length != 0) {
	$("#lines button").removeAttr("disabled");
    } else {
	$("#lines button").attr("disabled", true);
    }
});

$("#lines").submit(function(event) {
    event.preventDefault();
    var v = [$("#linesFilter").val(), $("#line").val(), $("#stations").val(), $("#destinations").val().length != 0?$("#destinations").val():""]
    history.pushState(null, $("title").text(), "/" + v.join("/"));
    main(v, false);
});

function findLines(type) {
    $.getJSON( BASE_API + "/lines/" + type, function( data ) {
	$("#line").empty();
	$("#line").append("<option/>");
	$.each( data["result"][type], function( key, val ) {
	    $( "<option/>", { text: val["name"] + " (" + val["directions"] + ")", value: val["code"]} ).appendTo("#line");
	});
	$("#line").removeAttr("disabled");

	if (uri.length == 2) {
	    $("#line").val(uri[1]);
	    $("#line").change();
	}
    });
}

function findDestinations(type, code) {
    $.getJSON( BASE_API + "/destinations/" + type + "/" + code, function( data ) {
	$("#destinations").empty();
	$("#destinations").append("<option/>");
	$.each( data["result"]["destinations"], function( key, val ) {
	    $( "<option/>", { text: val["name"], value: val["way"]} ).appendTo("#destinations");
	});
	$("#destinations").removeAttr("disabled");
    });
}

function findStations(type, code) {
    $.getJSON( BASE_API + "/stations/" + type + "/" + code, function( data ) {
	$("#stations").empty();
	$("#stations").append("<option/>");
	$.each( data["result"]["stations"], function( key, val ) {
	    $( "<option/>", { text: val["name"], value: val["slug"]} ).appendTo("#stations");
	});
	$("#stations").removeAttr("disabled");
    });
}

function getTraffic(type, code) {
    $("#traffic").hide();
    if (nextTraffic != null) {
	clearTimeout(nextTraffic);
	nextTraffic = null;
    }

    $.getJSON( BASE_API + "/traffic/" + type + "/" + code, function( data ) {
	$("#traffic h3").text(data["result"]["title"]);
	$("#traffic p").html(data["result"]["message"]);
	$("#traffic").show();

	nextTraffic = setTimeout(function() {
	    getSchedule(type, code);
	}, 150000);
    });
}

function getSchedule(type, code, station, way) {
    if (nextSchedule != null) {
	clearTimeout(nextSchedule);
	nextSchedule = null;
    }

    $("#schedule").hide();
    $.getJSON( BASE_API + "/schedules/" + type + "/" + code + "/" + station + "/" + way, function( data ) {
	$("#schedule dl").empty();
	$.each( data["result"]["schedules"], function( key, val ) {
	    $("<dt/>", { text: val["destination"] }).appendTo("#schedule dl");
	    $("<dd/>", { text: val["message"] }).appendTo("#schedule dl");
	});
	$("#schedule").show();

	updateTime(new Date(data["_metadata"]["date"]));

	nextSchedule = setTimeout(function() {
	    getSchedule(type, code, station, way);
	}, 15000);
    })
	.fail(function(data) {
	    if (data.responseJSON && data.responseJSON.result && data.responseJSON.result.message)
	    {
		$("#mainerror").text(data.responseJSON.result.message);
		$("#mainerror").show();
	    }
	});
}

function getStationName(station) {
    return station.replace(/\+/g, " ").replace(/\b[a-z]/g, function(letter) {
	return letter.toUpperCase();
    }).replace(/\b(de|du|sur)\b/gi, function(word) {
	return word.toLowerCase();
    });
}

function defineTag(type, station) {
    station = getStationName(station);
    $("#station").text(station);
    $(".navbar-brand").hide();
    $("#station").show();
    $("title").text("RATP - Prochains passages à " + (type == "bus" ? "l'arrêt" : "la station") + " " + station);
}

function updateTime(time) {
    var hours = time.getHours();
    var minutes = time.getMinutes();

    $("#update").text(hours + ":" + (minutes < 10 ? '0' + minutes : minutes));
}
updateTime(new Date());

function viewRecents(type) {
    $("#recents").empty();
    var recents = JSON.parse(localStorage.recents);
    $.each( Object.keys(recents).sort(function(a,b){return recents[b]-recents[a]}), function( key, val ) {
	if (recents[val] > 1) {
	    var s = val.split("/");
	    $("<li class='page-item'/>", {html: $( "<a/>", { href: "/" + val, "class": "page-link", text: s[0][0].toUpperCase() + s[1] + " : " + getStationName(s[2]) } ).bind("click", function(e) { main(val.split("/"), true); history.pushState(null, "title 1", "/" + val); return false; }).appendTo("#recents")});
	}
    });
    $("#recents").show();
}

function main(uri, fetchTrafic) {
    $("#lines").hide();
    $("#line").attr("disabled", true);
    $("#destinations").attr("disabled", true);
    $("#stations").attr("disabled", true);
    $("#schedule").hide();
    $("#loading").hide();
    $("#mainerror").hide();
    $("#recents").hide();

    if (nextTraffic != null) {
	clearTimeout(nextTraffic);
	nextTraffic = null;
    }
    if (nextSchedule != null) {
	clearTimeout(nextSchedule);
	nextSchedule = null;
    }

    if (localStorage.getItem("recents") === null)
	localStorage.setItem("recents", JSON.stringify({}));

    if (uri.length == 4 || uri.length == 3) {
	if (uri[3] == "")
	    uri.pop();
	if (uri.length == 3)
	    uri.push("A+R");

	defineTag(uri[0], uri[2]);
	getSchedule(uri[0], uri[1], uri[2], uri[3]);

	if (fetchTrafic)
	    getTraffic(uri[0], uri[1]);

	mpath = uri.slice(0,3);
	recents = JSON.parse(localStorage.recents);
	if (recents[mpath.join("/")] === undefined)
	    recents[mpath.join("/")] = 0;
	recents[mpath.join("/")] += 1;
	localStorage.recents = JSON.stringify(recents);
    } else if (uri.length == 1 || uri.length == 2) {
	$("#traffic").hide();
	$("#station").hide();
	$("title").text("Application RATP");

	switch(uri[0]) {
	case "metros":
	case "rers":
	case "bus":
	case "tramways":
	case "noctiliens":
	    $("#lines").show();
	    $("#linesFilter").val(uri[0]);
	    $("#linesFilter").change();
	    break;
	default:
	    window.location.pathname = "/";
	}
	viewRecents(uri[0]);
    } else if (uri.length == 0) {
	if ($("#linesFilter").val().length != 0)
	    main([ $("#linesFilter").val() ], true);
	else
	    $("#lines").show();
	viewRecents();
    }
}
main(uri, true);

window.onpopstate = function(event) {
    main(window.location.pathname.split("/").filter(function(v) {
	return v.length > 0;
    }), true);
}

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(function(registration) {
	console.log('Service worker registration succeeded:', registration);
    }).catch(function(error) {
	console.log('Service worker registration failed:', error);
    });
}
